using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{

    public GameObject _CanvasMainMenu, _Score, _Settings;

    public void StartGame()
    {
        SceneManager.LoadScene("SampleScene");
    }
    public void Exit()
    {
        Application.Quit();
    }
    public void BackToMenu()
    {
        _CanvasMainMenu.SetActive(true);
    }
    public void ShowScore()
    {
        _Score.SetActive(true);
        _CanvasMainMenu.SetActive(false);
    }
    public void CloseScore()
    {
        _Score.SetActive(false);
        _CanvasMainMenu.SetActive(true);
    }
    public void ShowSettings()
    {
        _Settings.SetActive(true);
        _CanvasMainMenu.SetActive(false);
    }
    public void CloseSettings()
    {
        _Settings.SetActive(false);
        _CanvasMainMenu.SetActive(true);
    }
}
