using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;

public class SaveAndLoadScore : MonoBehaviour
{
    public Text scoreText, addScoreBest1, addScoreBest2, addScoreBest3;
    public Text nameText, addNameBest1, addNameBest2, addNameBest3;
    int ActualScore, toSaveFirstBestScore, toSaveSecondBestScore, toSaveThirdBestScore;
    string ActualName, toSavePlayersNameBest1, toSavePlayersNameBest2, toSavePlayersNameBest3;

    public void IsBetterActualScoreThen()
    {
        ActualScore = Convert.ToInt32(scoreText.text);
        if (nameText.text != null)
        {
            ActualName = nameText.text;
        }
        else
        {
            ActualName = "�����";
        }
        LoadGame();
        if (ActualScore > toSaveFirstBestScore)
        {
            toSaveThirdBestScore = toSaveSecondBestScore;
            toSavePlayersNameBest3 = toSavePlayersNameBest2;

            toSaveSecondBestScore = toSaveFirstBestScore;
            toSavePlayersNameBest2 = toSavePlayersNameBest1;

            toSaveFirstBestScore = ActualScore;
            toSavePlayersNameBest1 = ActualName;
        }
        else if (ActualScore > toSaveSecondBestScore)
        {
            toSaveThirdBestScore = toSaveSecondBestScore;
            toSavePlayersNameBest3 = toSavePlayersNameBest2;

            toSaveSecondBestScore = ActualScore;
            toSavePlayersNameBest2 = ActualName;
        }
        else if (ActualScore > toSaveThirdBestScore)
        {
            toSaveThirdBestScore = ActualScore;
            toSavePlayersNameBest3 = ActualName;
        }
        SaveGame();
    }
    void SaveGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath
          + "/MySaveDataTower.dat");
        SaveData data = new SaveData();
        data.savedFirstBestScore = toSaveFirstBestScore;
        data.savedSecondBestScore = toSaveSecondBestScore;
        data.savedThirdBestScore = toSaveThirdBestScore;

        data.savedPlayersNameBest1 = toSavePlayersNameBest1;
        data.savedPlayersNameBest2 = toSavePlayersNameBest2;
        data.savedPlayersNameBest3 = toSavePlayersNameBest3;

        bf.Serialize(file, data);
        file.Close();
    }
    void LoadGame()
    {
        if (File.Exists(Application.persistentDataPath
          + "/MySaveDataTower.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/MySaveDataTower.dat", FileMode.Open);
            SaveData data = (SaveData)bf.Deserialize(file);
            file.Close();
            toSaveFirstBestScore = data.savedFirstBestScore;
            toSaveSecondBestScore = data.savedSecondBestScore;
            toSaveThirdBestScore = data.savedThirdBestScore;

            toSavePlayersNameBest1 = data.savedPlayersNameBest1;
            toSavePlayersNameBest2 = data.savedPlayersNameBest2;
            toSavePlayersNameBest3 = data.savedPlayersNameBest3;
        }
    }

    public void ShowScore()
    {
        LoadGame();

        if (toSavePlayersNameBest1 != null)
        {
            addNameBest1.text = toSavePlayersNameBest1;
        }
        else
        {
            addNameBest1.text = "�����";
        }
        if (toSavePlayersNameBest2 != null)
        {
            addNameBest2.text = toSavePlayersNameBest2;
        }
        else
        {
            addNameBest2.text = "�����";
        }
        if (toSavePlayersNameBest3 != null)
        {
            addNameBest3.text = toSavePlayersNameBest3;
        }
        else
        {
            addNameBest3.text = "�����";
        }

        addScoreBest1.text = "����� �������: " + toSaveFirstBestScore;
        addScoreBest2.text = "����� �������: " + toSaveSecondBestScore;
        addScoreBest3.text = "����� �������: " + toSaveThirdBestScore;
    }
    public void IsNull(String _text, String _string)
    {
        if (_string != null)
        {
            _text = _string;
        }
        else
        {
            _text = "�����";
        }
    }
}

[Serializable]
class SaveData
{
    public int savedFirstBestScore, savedSecondBestScore, savedThirdBestScore;
    public string savedPlayersNameBest1, savedPlayersNameBest2, savedPlayersNameBest3;
}
