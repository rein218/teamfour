using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOutOfScene : MonoBehaviour
{
    GameManager gameManager;
    public GameObject hp;
    void Awake()
    {
        gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Block") 
        {
            if (hp.transform.childCount > 1) //������� ���������
            {                
                Destroy(hp.transform.GetChild(0).gameObject);                
            }
            else
            {
                Destroy(hp.transform.GetChild(0).gameObject);
                gameManager.GameOver();
            }
            Destroy(other.gameObject);
            gameManager.LoseScore();
            if (gameManager.stForce <= 4)
            {
                gameManager.stForce++;
            }
            gameManager.CreateBlock();
            gameManager.RecountTime();
        }
        else if (other.gameObject.tag == "Tower")
        {
            Destroy(other.gameObject);
        }
    }
}
