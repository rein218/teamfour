using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SoundVolumeSettings : MonoBehaviour
{
    private float musicVolume = 0.5f, soundVolume = 0.5f;
    public bool IsSoundOn;
    public Slider musicSlider, soundSlider;
    public Toggle musicToggle, soundToggle;
    AudioSource mainMusic;
    private void Awake()
    {
        mainMusic = GetComponent<AudioSource>();
        if (PlayerPrefs.HasKey("SavedMusicVolume"))
        {
            musicVolume = PlayerPrefs.GetFloat("SavedMusicVolume");
            musicSlider.value = musicVolume;
        }
        if (PlayerPrefs.HasKey("SavedSoundVolume"))
        {
            soundVolume = PlayerPrefs.GetFloat("SavedSoundVolume");
            soundSlider.value = soundVolume;
        }
        if (PlayerPrefs.HasKey("IsMusicON"))
        {
            if (PlayerPrefs.GetInt("IsMusicON")==1)
            {
                musicToggle.isOn = true;
            }
            else
            {
                musicToggle.isOn = false;
            }            
        }
        if (PlayerPrefs.HasKey("IsSoundON"))
        {
            if (PlayerPrefs.GetInt("IsSoundON") == 1)
            {
                soundToggle.isOn = true;
            }
            else
            {
                soundToggle.isOn = false;
            }
        }

    }
    public void SetOnOffMusic()
    {
        if (!musicToggle.isOn)
        {
            mainMusic.Pause();
            PlayerPrefs.SetInt("IsMusicON", 0);
            PlayerPrefs.Save();
        }
        else
        {
            mainMusic.Play();
            PlayerPrefs.SetInt("IsMusicON", 1);
            PlayerPrefs.Save();
        }
    }
    public void SetOnOffSound()
    {
        if (!soundToggle.isOn)
        {
            PlayerPrefs.SetInt("IsSoundON", 0);
            PlayerPrefs.Save();
        }
        else
        {
            PlayerPrefs.SetInt("IsSoundON", 1);
            PlayerPrefs.Save();
        }
    }
    public void SetVolumeMusic()
    {
        musicVolume = musicSlider.value;
        mainMusic.volume = musicVolume;
        PlayerPrefs.SetFloat("SavedMusicVolume", musicVolume);
        PlayerPrefs.Save();
    }
    public void SetVolumeSound()
    {
        soundVolume = soundSlider.value;
        PlayerPrefs.SetFloat("SavedSoundVolume", soundVolume);
        PlayerPrefs.Save();
    }
}
