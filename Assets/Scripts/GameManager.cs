using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	public GameObject _PrafabBlock, _PrafabBlock1, _PrafabBlock2, _PrafabBlock3, towerObj, hook, 
		shakeObj, connectedBlock = null, chosedBlock = null, pauseMenu, hud, hp, endGame;
	public float  stForce = 0f, stDirection = 1f, lastTowerPositionY, shakingForce = 5f, soundVolume=0.5f;
	private float score = 0, scoreOfBlocks, combo = 0, timeBorderToTowerShake = 7f, timeStart = 7f;
	public bool moveTower = false, countTime=true, isSoundOn=true;
	public Text scoreText, scoreTextInMenu, scoreBlocksInMenu, comboText, scoreTextInEnd, scoreBlocksInEnd;
	Rigidbody rbConnectedBlock;
	public AudioClip comboSound, loseBlockSound, goToTowerSound;
	private AudioSource AudioSource;

	void Awake()
	{		
		AudioSource = GetComponent<AudioSource>();
		CheckSound();
		hook.transform.position = new Vector3(Random.Range(-4, 4), hook.transform.position.y, hook.transform.position.z);
		chosedBlock = _PrafabBlock2;
		CreateBlock();
	}

    void Update()
	{
		if (Input.GetButtonDown("Jump") && FindObjectOfType<ConfigurableJoint>().connectedBody != null && !moveTower )
		{			
			FallBlock();
		}
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Pause();
		}
		if (countTime)
		{
			timeStart -= Time.deltaTime;
		}
		if (stForce <= 4 && timeStart<0)
		{
			stForce++;
			RecountTime();
		}
	}

	void FixedUpdate()
	{
		if (FindObjectOfType<ConfigurableJoint>().connectedBody != null)
		{		
			ShakingBlock();
		}
		if (stForce > 0f)
		{
			ShakingTower();			
		}

	}

	//�������� �� ����� �����
	public void FallBlock()
    {
		countTime = false;
		rbConnectedBlock.velocity = Vector3.zero;
		rbConnectedBlock.angularVelocity = Vector3.zero;
		float rot = (int)connectedBlock.transform.rotation.eulerAngles.z;
		if (rot > 180)
		{
			rot = -360 + rot;
		}
		if (rot < 45 && rot > -45)
		{
			connectedBlock.transform.rotation = Quaternion.Euler(0, 180, 0);
		}
		else if (rot > 45)
		{
			connectedBlock.transform.rotation = Quaternion.Euler(0, 180, 90);
		}
		else if (rot < -45)
		{
			connectedBlock.transform.rotation = Quaternion.Euler(0, 180, -90);
		}
		hook.transform.position = new Vector3(Random.Range(-4, 4), hook.transform.position.y, hook.transform.position.z);
		FindObjectOfType<ConfigurableJoint>().connectedBody = null;		
	}

	public void RecountTime()
    {
		timeStart = timeBorderToTowerShake;
		countTime = true;
	}
	//�������� �����			
	public void CreateBlock()
	{
		//� ��������� ������ �� 180, ������ �� ��� ���������� ��������� �����, ���� � ������� ����� �������� �� 180
		connectedBlock = Instantiate(chosedBlock, new Vector3(hook.transform.position.x + 3.5f, hook.transform.position.y + 1, 0), Quaternion.Euler(0, 180, -90));
		rbConnectedBlock = connectedBlock.GetComponent<Rigidbody>();
		FindObjectOfType<ConfigurableJoint>().connectedBody = rbConnectedBlock;		
		chosedBlock = ChooseRandomBlock();
		timeBorderToTowerShake = timeBorderToTowerShake*0.95f;
	}
	

	//��������� ����� �����
	public GameObject ChooseRandomBlock()
	{
		int rundomNum = Random.Range(1, 4);
		if (rundomNum == 1)
        {
			if (chosedBlock!= _PrafabBlock)
            {
				return _PrafabBlock;
			}
			return ChooseRandomBlock();
		}
		else if (rundomNum == 2)
        {
			if (chosedBlock != _PrafabBlock1)
			{
				return _PrafabBlock1;
			}
			return ChooseRandomBlock();
		}
		else if (rundomNum == 3)
		{
			if (chosedBlock != _PrafabBlock2)
			{
				return _PrafabBlock2;
			}
			return ChooseRandomBlock();
		}
		else 
		{
			if (chosedBlock != _PrafabBlock3)
			{
				return _PrafabBlock3;
			}
			return ChooseRandomBlock();
		}

	}

	//������ ����			
	void ShakingBlock()
	{
		if (hook.transform.position.x >= 7)
		{
			shakingForce = -5f;
		}
		else if (hook.transform.position.x <= -7)
		{
			shakingForce = 5f;		
		}
		hook.transform.position = new Vector3(hook.transform.position.x + shakingForce * Time.deltaTime, hook.transform.position.y, hook.transform.position.z);

	}

	//������ �����, ���� ����� ���������
	void ShakingTower()
	{
        if (shakeObj.transform.eulerAngles.z >= 2 * stForce && shakeObj.transform.eulerAngles.z < 180f)
        {
            stDirection = -1f;
        }
        else if (shakeObj.transform.eulerAngles.z <= 360 - 2 * stForce && shakeObj.transform.eulerAngles.z > 180f)
        {
            stDirection = 1f;
        }
		shakeObj.transform.Rotate(0f, 0f, Time.deltaTime * stDirection * stForce);
    }

	//�������� �����
	public void AddScore()
	{
		if (isSoundOn)
        {
			AudioSource.PlayOneShot(goToTowerSound);
		}
		
		score += 5 + combo - combo % 5;		
		scoreOfBlocks++;
		combo++;
        if (combo % 5 == 0)
        {
			if (isSoundOn)
			{
				AudioSource.PlayOneShot(comboSound);
			}			
			if (stForce >= 0 && stForce <= 4)
            {
                stForce--;
            }
        }
        comboText.text = "X" + (5 + combo - combo % 5) / 5;
		scoreText.text = "" + score;		
	}

	//������ �����
	public void LoseScore()
    {
		if (isSoundOn)
        {
			AudioSource.PlayOneShot(loseBlockSound);
		}		
		score -= 10;
		combo=0;
		comboText.text = "X" + (5 + combo - combo % 5) / 5;
		scoreText.text = "" + score;
	}

	public void Pause()
	{
		if (!pauseMenu.active)
		{
			Time.timeScale = 0;
			pauseMenu.SetActive(true);
			hud.SetActive(false);
			scoreTextInMenu.text = "����� �������:  " + score;
			scoreBlocksInMenu.text = "������ �����������:  " + scoreOfBlocks;
		}
		else 
		{
			ResumePlay();
		}
	}
	public void GameOver()
	{
		Time.timeScale = 0;
		endGame.SetActive(true);
		hud.SetActive(false);		
		scoreTextInEnd.text = "����� �������:  " + score;
		scoreBlocksInEnd.text = "������ �����������:  " + scoreOfBlocks;
		//SaveData();
	}

	public void ExitToMenu()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene("Menu");
	}

	public void ResumePlay()
	{
		CheckSound();
		if (hp.transform.childCount >= 1)
		{
			Time.timeScale = 1;
			pauseMenu.SetActive(false);
			hud.SetActive(true);
		}
	}
	public void CheckSound()
    {
		if (PlayerPrefs.HasKey("IsSoundON"))
		{
			if (PlayerPrefs.GetInt("IsSoundON") == 1)
			{
				isSoundOn = true;
			}
			else
			{
				isSoundOn = false;
			}

		}
		if (PlayerPrefs.HasKey("SavedSoundVolume"))
		{
			soundVolume = PlayerPrefs.GetFloat("SavedSoundVolume");
		}
		AudioSource.volume = soundVolume;
	}

}


