using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTower : MonoBehaviour
{
    GameManager gameManager;
    public GameObject hook;
    void Start()
    {
        gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (FindObjectOfType<ConfigurableJoint>().connectedBody != null)
        {
            gameManager.towerObj.transform.localPosition = new Vector3(0, gameManager.towerObj.transform.localPosition.y - 0.2f, 0);
        }
         
    }
}
