using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockFalling : MonoBehaviour
{
    Rigidbody rbThis;
    public bool isTowered = false, touched = false, isSoundOnBlock;
    public float collideAngle, time = 3f, soundVolumeBlock;
    public AudioClip touchedTowerSound;
    private AudioSource AudioSource;
    GameObject towerObj;
    GameManager gameManager;

    void Start()
    {
        AudioSource=GetComponent<AudioSource>();        
        towerObj = GameObject.FindWithTag("Tower");
        rbThis = GetComponent<Rigidbody>();
        gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();        
    }

    //�������� ���� ����� ����� �� ����� ��� �� plane
    void OnCollisionEnter(Collision touchedObj)
    {        
        if (!isTowered)
        {
            //� �� ����, ��� � ���� ������ ��� ��� ������� ��� ����, ��� ��������� �������
            soundVolumeBlock = gameManager.GetComponent<GameManager>().soundVolume;
            isSoundOnBlock = gameManager.GetComponent<GameManager>().isSoundOn;
            if (isSoundOnBlock) 
            {
                AudioSource.volume = soundVolumeBlock;
                AudioSource.PlayOneShot(touchedTowerSound);
            }            
            transform.parent = towerObj.transform;
            rbThis.velocity = Vector3.zero;
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (!isTowered)
        {
            touched = true;
        }
    }

    //���� ���� ����� ������ � ����������� � ������ �� �� �������, �� ������������ ����� ����������
    private void OnCollisionExit(Collision collision)
    {
        touched = false;

        time = 3f;
    }

    //���������� ���� �� �����
    void GetToTower()
    {
        rbThis.isKinematic = true;
        gameObject.tag = "Tower";
        isTowered = true;
        gameManager.AddScore();
        time = 3f;
        touched = false;
        gameManager.CreateBlock();
        gameManager.RecountTime();
    }

    void FixedUpdate()
    {
        if (touched)
        {
            time = time - Time.deltaTime;
            if (time <= 0)
            {
                //�������� � ���� �� � ��� � �����    - ��� ������� :)
                gameManager.lastTowerPositionY = towerObj.transform.position.y;
                GetToTower();
                touched = false;                
            }
        }
    }

}

